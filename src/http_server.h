//
//  http_server.h
//  skynet_recorder
//
//  Created by Miha Nedok on 27/09/16.
//  Copyright © 2016 Mike. All rights reserved.
//

#ifndef http_server_h
#define http_server_h

#include "global.h"

BEGIN_EXTERN_C()

gboolean http_server_init();

END_EXTERN_C();

#endif /* http_server_h */
