
#ifndef _GLOBAL_H
#define _GLOBAL_H

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <locale.h>


#include <gst/gst.h>

#include <glib-2.0/glib.h>
#include <glib.h>
#include <jansson.h>
#include <zmq.h>
#include <zlib.h>
#include <syslog.h>
#include <curl/curl.h>


#include "SkynetLib.h"


#include "http_server.h"


#define BLOCK_OF_1K 1024UL
#define BLOCK_OF_8K 8*BLOCK_OF_1K

typedef struct restreamer_config_s{
  char *vpnIp;
  int vpnPort;
  char *publicIp;
  char *application_id;
  char *skynet_username;
  char *skynet_password;
  char *application_session_id;
  char *skynet_connect_uri;
  /**
   *  Also nodename
   */
  char *instanceName;
  char *storagePath;
}restreamer_config_s;
typedef restreamer_config_s* restreamer_config_p;

typedef struct active_recording_s{
  char *media_session_id;
  char *recording_path;
}active_recording_s;
typedef active_recording_s* active_recording_p;


#define STREAM_TYPE_LOCAL_PLAYBACK  2000 * 1000


typedef struct streamer_gst_info_s{
  GstElement *pipeline;
  GstBus      *bus;
}streamer_gst_info_s;
typedef streamer_gst_info_s* streamer_gst_info_p;


#define STREAMER_TYPE_RESTREAM_LOCAL_FILE           100*100;
#define STREAMER_TYPE_RESTREAM_HTTP                 100*200;
#define STREAMER_TYPE_RESTREAM_UDP                  100*300;
#define STREAMER_TYPE_RESTREAM_ASTRO_MEDIA_SESSION  100*400;

/**
 *  Data for a single Stream
 */
typedef struct streamer_data_s{
  
  int streamerType;
  
  char *fileToStreamPath;
  char *streamerName;
  char *mediaSessionId;
  char *sourceStreamHost;
  int sourceStreamPort;
  int sourceStreamPort2;
  gboolean running;
  time_t startTime;
  
  GstBus *gst_bus;
  int streamerPort;
  
  streamer_gst_info_p streamer_gst;
  
}streamer_data_s;
typedef streamer_data_s* streamer_data_p;


/* True Global Resources */

extern restreamer_config_p app_config;
extern GMutex mutex_active_recordings;
extern GHashTable* table_active_streams;
extern GAsyncQueue* queue_streams;
extern GHashTable *cmdHashTable;
extern node_data_p node_data;
extern struct MHD_Daemon *http_server;


/* Global Functions */

/* Server Command List */
SKYNET_DECLARE_JSON_FUNCTION(cmd_info);



GstElement *videosrc_test();



struct SkynetServerCommand* skynet_lookup_command(const char *command);


#define MP_SET_SHUTDOWNHANDLER(handler){\
signal(SIGSTOP,handler);\
signal(SIGTERM,handler);\
signal(SIGKILL,handler);\
signal(SIGINT,handler);\
}


#define mpGFreePtr(ptr){if(ptr!=NULL){g_free(ptr);}}
#define mpFreeString(str)if(str!=NULL){g_string_free(str,TRUE);}}

void initLog();

#endif