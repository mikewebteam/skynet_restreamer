//
//  http_server.c
//  skynet_recorder
//
//  Created by Miha Nedok on 27/09/16.
//  Copyright © 2016 Mike. All rights reserved.
//

#include "http_server.h"


struct MHD_Daemon *http_server;


static GHashTable* methodRegister;


typedef void (*json_method_cb)(skynet_http_msg_p msg);

/*
 int
 http_process(void *cls,
 struct MHD_Connection *connection,
 const char *url,
 const char *method,
 const char *version,
 const char *upload_data,
 size_t *upload_data_size,
 void **con_cls,
 skynet_http_msg_p msg
 ){
 
 printf("[%s] [%s] [%s]\n",url,method,version);
 if (msg->j_request)
 printf("\n[%s]\n",json_dumps(msg->j_request,JSON_INDENT(2)));
 skynet_http_return_success(msg,"test", 1,NULL);
 return MHD_YES;
 }
 */

static int http_process(void *cls,
                        struct MHD_Connection *connection,
                        const char *url,
                        const char *method,
                        const char *version,
                        const char *upload_data,
                        size_t *upload_data_size,
                        void **con_cls,
                        skynet_http_msg_p msg
                        ){
  
  printf("[%s] [%s] [%s]\n",url,method,version);
  printf("Params:\n");
  skynet_http_msg_dump_key_val(msg->req_params);
  printf("Headers:\n");
  skynet_http_msg_dump_key_val(msg->req_headers);
  printf("Raw Body: %s\n",msg->raw_body->str);
  if (msg->j_request){
    char *dump = json_dumps(msg->j_request,0);
    printf("Reencode: %s\n",dump);
    char *methodName = J_OBJ_GET_STR(msg->j_request,"methodName");
    if (methodName){
      void *pfunc = (gpointer)g_hash_table_lookup(methodRegister,methodName);
      if (pfunc){
        
      }
    }
  }
  skynet_http_return_success(msg,"test", 1,NULL);
  return MHD_YES;
}

static void json_method_requestRestream(skynet_http_msg_p msg){
  printf("IN CALLBACKER");
}


gboolean http_server_init(){
  printf("P: [%d]\n",app_config->vpnPort);
  http_server = skynet_http_create_server(app_config->vpnIp,app_config->vpnPort,"/",NULL,&http_process);
  methodRegister = G_STR_HASH_TABLE_NEW;
  //  g_hash_table_insert(methodRegister,(gpointer)"requestRestream",(gpointer)&json_method_requestRestream);
  return TRUE;
}

