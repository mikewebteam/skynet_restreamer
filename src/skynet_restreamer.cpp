//
//  main.c
//  SkynetMediaTranscoder
//
//  Created by Miha Nedok on 25/09/16.
//  Copyright © 2016 Mike. All rights reserved.
//

#include "global.h"



restreamer_config_p app_config;

node_data_p node_data;



GMainLoop *loop;


GHashTable* table_active_streams;
GAsyncQueue* queue_streams;

GAsyncQueue* queue_stop_streams;

GThread* watchdog_streams_thread;

typedef struct streams_table_entry {
  GstElement *pipeline;
  GstBus      *bus;
  char *mediaSessionId;
}streams_table_entry;

typedef streams_table_entry* streams_table_entry_p;




GstElement *videosrc_test(){
  return gst_element_factory_make("videotestsrc","my_video_test_src");
}


static void signal_handler(int sig){
  if (sig == SIGABRT || sig == SIGTERM || sig == SIGKILL || sig == SIGINT || sig == SIGSTOP){
    syslog(LOG_NOTICE,"Received shutdown signal: [%d]",sig);
    g_main_loop_quit(loop);
  }
}

static void _foreach_watchdog_streams(gpointer key, gpointer value, gpointer userdata){
  //char* remove_id = (char*)g_async_queue_pop(queue_stop_streams);
  printf("Key: [%s]\n",key);
  //if (remove_id)
  //    printf("To Remove: [%s]\n",remove_id);
}
static gpointer streams_watchdog(gpointer data){
  while(TRUE){
    printf("Watchdog started...\n");
    /*
     if (g_async_queue_length(queue_stop_streams)>0){
     char* remove_id = (char*)g_async_queue_pop(queue_stop_streams);
     if (remove_id){
     GThread *thread_to_remove = (GThread*)g_hash_table_lookup(table_active_streams,(gconstpointer)remove_id);
     g_thread_join(thread_to_remove);
     g_hash_table_remove(table_active_streams,(gconstpointer)remove_id);
     g_free(remove_id);
     }
     }
     g_hash_table_foreach(table_active_streams,(GHFunc)_foreach_watchdog_streams,NULL);
     */
    g_usleep(5 * TIME_MICRO_IN_SEC);
  }
  return NULL;
}


static void stream_destroy_notif(gpointer item){
  syslog(LOG_INFO,"Stream Destroyed...");
}

static gboolean gst_buss_callback(GstBus *bus,GstMessage *msg,gpointer user_data){
  
  
  return TRUE;
}

static gpointer streamer_thread(gpointer data){
  streamer_data_p streamer_data = (streamer_data_p)data;
  GstElement *pipeline, *filesrc, *decoder, *filter, *sink;
  GstElement *convert1, *convert2, *resample;
  GMainLoop *loop;
  GstBus *bus;
  guint watch_id;
  
  streamer_data->streamer_gst->pipeline = gst_pipeline_new(streamer_data->mediaSessionId);
  streamer_data->streamer_gst->bus      = gst_pipeline_get_bus(GST_PIPELINE(streamer_data->streamer_gst->pipeline));
  
  gst_bus_add_watch(streamer_data->streamer_gst->bus,&gst_buss_callback,(gpointer)streamer_data);
  
  while(true){
    printf("Working: [%s]\n",streamer_data->mediaSessionId);
    g_usleep(15 * TIME_MICRO_IN_SEC);
  }
  return NULL;
}

int main(int argc, char * argv[]) {
  GError *err;
  setlocale(LC_ALL, "en_US.utf8");
  setlogmask (LOG_UPTO (LOG_DEBUG));
  openlog("skynet_restreamer",LOG_NOWAIT|LOG_CONS | LOG_PID | LOG_NDELAY,LOG_SYSLOG);
  MP_SET_SHUTDOWNHANDLER(signal_handler);
  
  json_error_t j_error_t;
  json_t *j_config = json_load_file("restreamer.json",0,&j_error_t);
  if (!j_config){
    syslog(LOG_ERR,"Cannot Load Config: [%s]",j_error_t.text);
    fprintf(stderr,"Cannot Load Config: [%s]\n",j_error_t.text);
    return -1;
  }
  
  gst_init (&argc, &argv);
  app_config = g_new0(restreamer_config_s,1);
  
  // app_config->instanceName = J_OBJ_GET_STR_COPY(j_config,"instanceName");
  app_config->publicIp            = J_OBJ_GET_STR_COPY(j_config,"publicIp");
  app_config->vpnIp               = J_OBJ_GET_STR_COPY(j_config,"vpnIp");
  app_config->storagePath         = J_OBJ_GET_STR_COPY(j_config,"storagePath");
  app_config->instanceName        = SkynetGetHostname();
  app_config->vpnPort             = (int)J_OBJ_GET_INT(j_config,"vpnPort");
  app_config->skynet_password     = J_OBJ_GET_STR_COPY(j_config,"skynet_password");
  app_config->skynet_username     = J_OBJ_GET_STR_COPY(j_config,"skynet_username");
  app_config->skynet_connect_uri  = J_OBJ_GET_STR_COPY(j_config,"skynetUri");
  app_config->application_id      = J_OBJ_GET_STR_COPY(j_config,"application_id");
  
  
  app_config->application_session_id = NULL;
  
  
  if (!app_config->instanceName || !app_config->publicIp
      || !app_config->vpnIp || !app_config->storagePath || app_config->vpnPort <=0){
    syslog(LOG_ERR,"Config incomplete!");
    fprintf(stderr,"Config incomplete!\n");
    return -1;
  }
  
  
  // Release Json Conf
  json_decref(j_config);
  
  
  skynet_connect_init();
  
  if (skynet_connect_login_app(app_config->skynet_connect_uri,app_config->skynet_username,app_config->skynet_password,app_config->application_id,&app_config->application_session_id)){
    skynet_register_node("skynet_media",app_config->instanceName, SKYNET_REGISTER_TYPE_RESTREAMER, app_config->vpnIp, app_config->publicIp, app_config->skynet_connect_uri, app_config->application_id, app_config->application_session_id);
    skynet_heartbeat_sender("skynet_media",app_config->instanceName,SKYNET_REGISTER_TYPE_RESTREAMER, app_config->skynet_connect_uri, app_config->application_id, app_config->application_session_id, 10);
    http_server_init();
  } else {
    syslog(LOG_ERR,"Exiting - Skynet Login Failed!");
    return EXIT_FAILURE;
  }
  
  
  
  
  //GThread *pThread = g_thread_create(streamer_thread,(gpointer)streamer_data,TRUE,&err);
  
  //   g_thread_new("Streamer Thread",&streamer_thread,(gpointer)streamer_data);
  
  /*
   char *lastId = NULL;
   for ( int i = 0; i < 5; i++){
   streamer_data_p streamer_data = g_new(streamer_data_s, 1);
   streamer_data->mediaSessionId = genuuid();
   streamer_data->running        = FALSE;
   GThread *pThread = g_thread_create(streamer_thread,(gpointer)streamer_data,TRUE,&err);
   g_hash_table_insert(table_active_streams,streamer_data->mediaSessionId,(gpointer)pThread);
   // printf("\nStreams in queue: %d\n",);
   lastId = streamer_data->mediaSessionId;
   }
   // printf("last id: [%s]\n",lastId);
   
   watchdog_streams_thread = g_thread_create(streams_watchdog,NULL,TRUE,&err);
   
   g_async_queue_push(queue_stop_streams,(gpointer)g_strdup(lastId));
   */
  
  
  loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(loop);
  return 0;
}
